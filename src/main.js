// es6 兼容
import 'babel-polyfill'
import es6promise from 'es6-promise'
es6promise.polyfill()

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// UI 框架
import Vant from 'vant'
Vue.use(Vant)

// 适配
import 'amfe-flexible'

// 接口
import * as apis from './api/index'
Vue.prototype.$http = apis

// 过滤器
import * as filters from './filters/index'
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

// 样式
import 'vant/lib/index.css';


Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
