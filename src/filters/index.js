export const toFloat = input => {
  return parseFloat(input).toFixed(2)
}