import { get, post } from '../utils/http'

export const postMsnCode = params => post('/api/user/sendMsn', params)

export const getMsnCode = params => get('/api/user/sendMsn', params)

