import axios from 'axios'

// 根据不同的环境配配置不同的url
axios.defaults.baseURL = process.env.VUE_APP_BASEURL
// 携带cooike
axios.defaults.withCredentials = true;
// 请求超时时间
axios.defaults.timeout = 20000;
// 请求拦截器
axios.interceptors.request.use(config => {
  // axios 请求头配置 如请求头 数据格式等
  return config
}, err => {
  return Promise.resolve(err)
})
// 响应拦截
axios.interceptors.response.use(response => {
  return Promise.resolve(response)
}, err => {
  return Promise.resolve(err.res)
})

// get
export const get = (url, params) => {
  return new Promise((resolve, reject) => {
    axios.get(url, params)
      .then(res => {
        resolve(res.data);
      })
      .catch(err => {
        reject(err.data)
      })
  });
}

// post
export const post = (url, params) => {
  return new Promise((resolve, reject) => {
    axios.post(url, params)
      .then(res => {
        resolve(res.data);
      })
      .catch(err => {
        reject(err.data)
      })
  });
}