// vue-3.0 配置
module.exports = {
  // 打包后的部署路径
  publicPath: '/',
  // 构建后的输出文件
  outputDir: 'dist',
  // 静态文件的输出文件
  assetsDir: 'static',
  // 是否在保存的时候使用 `eslint-loader` 进行检查。
  // 有效的值：`ture` | `false` | `"error"`
  // 当设置为 `"error"` 时，检查出的错误会触发编译失败。
  lintOnSave: true,
  // 是否使用包含运行时编译器的 Vue 构建版本
  runtimeCompiler: false,
  // 是否为生产环境构建生成 source map
  productionSourceMap: false,
  // 调整内部的 webpack 配置
  chainWebpack: () => { },
  configureWebpack: () => { },
  // CSS 相关选项
  css: {
    // 将组件内的 CSS 提取到一个单独的 CSS 文件 (只用在生产环境中)
    // 也可以是一个传递给 `extract-text-webpack-plugin` 的选项对象
    extract: true,
    // 是否开启 CSS source map
    sourceMap: false,
    // 为预处理器的 loader 传递自定义选项。比如传递给
    // sass-loader 时，使用 `{ sass: { ... } }`。
    loaderOptions: {},
    // 为所有的 CSS 及其预处理文件开启 CSS Modules。
    // 这个选项不会影响 `*.vue` 文件。
    modules: false
  },
  // 配置 webpack-dev-server 行为。
  devServer: {
    port: 8088,
    proxy: {
      '/api': {
        target: 'http://sale.51cubi.com',
        pathRewrite: { '^/api': '/' },
        changeOrigin: true,
        autoRewrite: true,
        cookieDomainRewrite: true,
      }
    }
  }
}